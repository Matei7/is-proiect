package com.Controller;


import com.Business.MatchBusiness;
import com.Business.TicketBusiness;
import com.Business.UserBusiness;
import com.DAO.UserDAO;
import com.Service.Enums;
import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;

@Controller
public class UserController {
    @Autowired
    UserDAO userRepository;
    @Autowired
    UserBusiness userBusiness;
    @Autowired
    MatchBusiness matchBusiness;
    @Autowired
    TicketBusiness ticketBusiness;


    @RequestMapping(value = "register", method = RequestMethod.POST)
    public String addUser(@RequestParam String username, @RequestParam String password,
                          @RequestParam String name, @RequestParam String addr, @RequestParam String email,
                          @RequestParam String cellphone) throws UnsupportedEncodingException {

        userBusiness.addUser(username, password, name, addr, email, cellphone, Enums.UserType.CUSTOMER);
        return "login";
    }


    @RequestMapping(value = "register", method = RequestMethod.GET)
    public String showCreateAccount() {
        return "register";
    }

    @RequestMapping(value = "addAdmin", method = RequestMethod.POST)
    public ModelAndView addAdmin(@RequestParam String username, @RequestParam String password,
                                 @RequestParam String name, @RequestParam String addr, @RequestParam String email,
                                 @RequestParam String cellphone, ModelMap model) throws UnsupportedEncodingException {

        userBusiness.addUser(username, password, name, addr, email, cellphone, Enums.UserType.ADMIN);
        model.addAttribute("matches", matchBusiness.getOpenMatches());
        return new ModelAndView("homepage2", model);
    }

    @RequestMapping(value = "addAdmin", method = RequestMethod.GET)
    public String addAdminPage() {
        return "addAdmin";
    }


    @RequestMapping(value = "/deposit", method = RequestMethod.GET)
    public String depositPage() {
        return "deposit";
    }


    @RequestMapping(value = "/deposit", method = RequestMethod.POST)
    public ModelAndView depositFunds(HttpServletRequest request, @RequestParam double amount, ModelMap model) {
        HttpSession session = request.getSession();
        userBusiness.depositFunds((User) session.getAttribute("name"), amount);
        model.addAttribute("matches",matchBusiness.getOpenMatches());


        User updatedUser = userRepository.findOne(((User) session.getAttribute("name")).getId());
        session.setAttribute("name",updatedUser);
        return new ModelAndView("homepage",model);
    }

    @RequestMapping(value = "/withdraw", method = RequestMethod.GET)
    public String withdrawPage() {
        return "withdraw";
    }


    @RequestMapping(value = "/withdraw", method = RequestMethod.POST)
    public ModelAndView withdrawFunds(HttpServletRequest request, @RequestParam double amount, ModelMap model) {

        HttpSession session = request.getSession();
        User member = (User) session.getAttribute("name");


        if (member.getBalance() < amount) {
            model.addAttribute("errorStatement", "Not enough money");
            return new ModelAndView("withdraw",model);
        }

        userBusiness.withdrawFunds(member, amount);

        User updatedUser = userRepository.findOne(member.getId());
        session.setAttribute("name",updatedUser);
        model.addAttribute("matches",matchBusiness.getOpenMatches());
        return new ModelAndView("homepage",model);
    }


    @RequestMapping(value = "/allUsers", method = RequestMethod.GET)
    public @ResponseBody
    Iterable<User> getAllUsers() {
        // This returns a JSON or XML with the users
        return userRepository.findAll();
    }

    @RequestMapping(value = "editUser", method = RequestMethod.GET)
    public String getEditPage(HttpServletRequest request, ModelMap model) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("name");
        model.addAttribute("user", user);

        return "editUser";
    }


    @RequestMapping(value = "editAdmin", method = RequestMethod.GET)
    public String getEditAdmin(HttpServletRequest request, ModelMap model) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("name");
        model.addAttribute("user", user);

        return "editAdmin";
    }

    @RequestMapping(value = "editUser", method = RequestMethod.POST)
    public ModelAndView updateUser(HttpServletRequest request, @RequestParam String name, @RequestParam String password,
                             @RequestParam String addr, @RequestParam String cellphone, ModelMap map) throws UnsupportedEncodingException {

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("name");
        userBusiness.updateUser(request, user.getId(), name, password, addr, cellphone);

        map.addAttribute("matches",matchBusiness.getOpenMatches());
        return new ModelAndView("homepage",map);
    }

    @RequestMapping(value = "editUser2", method = RequestMethod.POST)
    public ModelAndView updateAdmin(HttpServletRequest request, @RequestParam String name, @RequestParam String password,
                             @RequestParam String addr, @RequestParam String cellphone, ModelMap model) throws UnsupportedEncodingException {

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("name");
        userBusiness.updateUser(request, user.getId(), name, password, addr, cellphone);

        model.addAttribute("matches",matchBusiness.getOpenMatches());
        return new ModelAndView("homepage2",model);
    }

    @RequestMapping(value = "viewAccount", method = RequestMethod.GET)
    public String viewAccount(HttpServletRequest request, ModelMap model) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("name");
        User newUser = userBusiness.getUser(user.getId());
        session.setAttribute("user",newUser);
        model.addAttribute("user", newUser);
        return "viewAccount";
    }


    @RequestMapping(value = "checkBalance", method = RequestMethod.GET)
    public ModelAndView viewFinances(ModelMap model) {
        User user = userBusiness.getSuperUser();
        model.addAttribute("user", user);
        return new ModelAndView("checkBalance", model);
    }

    @RequestMapping(value = "checkBalance", method = RequestMethod.POST)
    public ModelAndView payTickets(ModelMap model) {
        int ticketsPaid = ticketBusiness.payTickets();
        User user = userBusiness.getSuperUser();

        model.addAttribute("ticketsPaid", ticketsPaid + " tickets were paid");
        model.addAttribute("user", user);
        return new ModelAndView("checkBalance", model);
    }
}
