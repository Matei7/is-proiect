package com.Controller;


import com.DAO.BetDAO;
import com.model.Bet;
import com.model.Match;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BetController {
    @Autowired
    BetDAO betRepository;

    @RequestMapping(value = "/allBets", method = RequestMethod.GET)
    public @ResponseBody Iterable<Bet> getAllBets() {
        // This returns a JSON or XML with the users
        return betRepository.findAll();
    }

}
