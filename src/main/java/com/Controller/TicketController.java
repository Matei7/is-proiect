package com.Controller;

import com.Business.MatchBusiness;
import com.Business.TicketBusiness;
import com.DAO.TicketDAO;
import com.model.Ticket;
import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class TicketController {
    @Autowired
    TicketBusiness ticketBusiness;

    @Autowired
    MatchBusiness matchBusiness;

    @Autowired
    TicketDAO ticketRep;

    @RequestMapping(value = "/allTickets", method = RequestMethod.GET)
    public @ResponseBody
    Iterable<Ticket> getAllBets() {
        // This returns a JSON or XML with the users
        return ticketRep.findAll();
    }


    @RequestMapping(value = "addToTicket", method = RequestMethod.GET)
    public ModelAndView addToTicket(HttpServletRequest request, @RequestParam int matchId, @RequestParam String odd, ModelMap map) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("name");

        if (ticketBusiness.existingMatch(user.getId(), matchId)) {
            map.addAttribute("errorStatement", "Bet already placed on this match. Choose another one");
        } else {
            ticketBusiness.addToTicket(user.getId(), matchId, odd);
        }
        map.addAttribute("odds",matchBusiness.getMatchesMap());

        return new ModelAndView("allMatches", map);
    }

    @RequestMapping(value = "confirmTicket",method = RequestMethod.GET)
    public ModelAndView getTicketContent(HttpServletRequest request, ModelMap model){
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("name");

        Ticket openTicket = ticketBusiness.getUserTicket(user);
        if(openTicket == null){
            model.addAttribute("matches", matchBusiness.getOpenMatches());
            return new ModelAndView("homepage",model);
        }
        model.addAttribute("content",ticketBusiness.getTicketContent(openTicket.getId()));
        model.addAttribute("ticketId",openTicket.getId());
        return new ModelAndView("confirmTicket",model);
    }

    @RequestMapping(value = "confirmTicket", method = RequestMethod.POST)
    public ModelAndView confirmTicket(HttpServletRequest request, @RequestParam double amount, @RequestParam int ticketId, ModelMap model){
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("name");

        if(user.getBalance()< amount){
            Ticket openTicket = ticketBusiness.getUserTicket(user);
            model.addAttribute("errorStatement","Insufficient funds");
            model.addAttribute("content",ticketBusiness.getTicketContent(openTicket.getId()));
            model.addAttribute("ticketId",openTicket.getId());
            return new ModelAndView("confirmTicket",model);
        }

        ticketBusiness.confirmTicket(ticketId, amount);
        model.addAttribute("matches", matchBusiness.getOpenMatches());
        return new ModelAndView("homepage",model);
    }
}
