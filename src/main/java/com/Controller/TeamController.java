package com.Controller;

import com.DAO.TeamDAO;
import com.model.Team;
import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeamController {

    @Autowired
    TeamDAO teamRepository;

    @RequestMapping(value = "/allTeams", method = RequestMethod.GET)
    public @ResponseBody Iterable<Team> getAllTeams() {
        // This returns a JSON or XML with the users
        return teamRepository.findAll();
    }
}
