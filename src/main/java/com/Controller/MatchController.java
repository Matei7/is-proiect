package com.Controller;


import com.Business.MatchBusiness;
import com.Business.TeamBusiness;
import com.DAO.MatchDAO;
import com.model.Match;
import com.model.User;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@RestController
public class MatchController {
    @Autowired
    MatchBusiness matchBusiness;

    @Autowired
    TeamBusiness teamBusiness;

    @RequestMapping(value = "/allMatches", method = RequestMethod.GET)
    public ModelAndView getAllMatches(ModelMap map) {

        map.addAttribute("odds",matchBusiness.getMatchesMap());
        return new ModelAndView("allMatches", map);
    }


    @RequestMapping(value = "addMatch", method = RequestMethod.GET)
    public ModelAndView addMatchPage(ModelMap map) {

        map.addAttribute("teams",teamBusiness.getTeams());
        return new ModelAndView("addMatch", map);
    }

    @RequestMapping(value = "addMatch", method = RequestMethod.POST)
    public ModelAndView addMatch(@RequestParam String homeTeam, @RequestParam String awayTeam, @RequestParam String startDate, ModelMap map) {

        matchBusiness.addMatch(homeTeam, awayTeam,startDate);
        map.addAttribute("teams",teamBusiness.getTeams());
        return new ModelAndView("addMatch", map);
    }


    @RequestMapping(value = "closeMatch", method = RequestMethod.GET)
    public ModelAndView closeMatchPage(ModelMap map) {

        map.addAttribute("matches",matchBusiness.getOpenMatches());
        return new ModelAndView("closeMatch", map);
    }


    @RequestMapping(value = "closeMatch", method = RequestMethod.POST)
    public ModelAndView closeMatch(ModelMap map, @RequestParam int matchId) {
        matchBusiness.simulateMatch(matchId);

        map.addAttribute("matches",matchBusiness.getOpenMatches());
        return new ModelAndView("closeMatch", map);
    }

    @RequestMapping(value = "history",method = RequestMethod.GET)
    public ModelAndView history(ModelMap map){
        map.addAttribute("matches",matchBusiness.getClosedMatches());
        return new ModelAndView("history",map);
    }




}
