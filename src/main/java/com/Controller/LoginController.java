package com.Controller;


import com.Business.MatchBusiness;
import com.Business.UserBusiness;
import com.Service.Enums;
import com.Service.PasswordHash;
import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Controller
public class LoginController {
    @Autowired
    UserBusiness userBusiness;
    @Autowired
    MatchBusiness matchBusiness;
    @Autowired
    PasswordHash passHash;

//    @RequestMapping(value = "/polybet", method = RequestMethod.GET)
//    public String notLoggedPage() {
//        return "notlogged";
//    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginPage() {
        return "login";
    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView handleLoginRequest(@RequestParam String username, @RequestParam String password, ModelMap model, HttpServletRequest request) throws UnsupportedEncodingException {
        User user = userBusiness.existingUser(username, passHash.getSha512SecurePassword(password, username.toLowerCase()));

        if (user != null) {
            HttpSession session = request.getSession();
            session.setAttribute("name", user);
            System.out.println(user.getType().toString());
            model.addAttribute("matches", matchBusiness.getOpenMatches());
            if(user.getType().equals(Enums.UserType.CUSTOMER)) {
                return new ModelAndView("homepage", model);
            }
            return new ModelAndView("homepage2", model);
        }

        model.addAttribute("error", "Invalid username/password");
        return new ModelAndView("login",model);

    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public void logout(HttpServletRequest request, HttpServletResponse httpResponse) throws IOException {
        if (request.getParameter("logout") != null) {
            HttpSession session = request.getSession(false);
            session.removeAttribute("name");
            session.invalidate();
        }
        httpResponse.sendRedirect("/login");
        return;
    }

    @GetMapping("/homepage")
    public ModelAndView getHomepage(ModelMap map) {
        map.addAttribute("matches", matchBusiness.getOpenMatches());
        return new ModelAndView("homepage",map);

    }

    @GetMapping("/homepage2")
    public ModelAndView getHomepage2(ModelMap map) {
        map.addAttribute("matches", matchBusiness.getOpenMatches());
        return new ModelAndView("homepage2",map);

    }

}
