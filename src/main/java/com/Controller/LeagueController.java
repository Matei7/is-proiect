package com.Controller;


import com.Business.TeamBusiness;
import com.DAO.LeagueDAO;
import com.model.League;
import com.model.Match;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class LeagueController {
    @Autowired
    TeamBusiness teamBusiness;

    @RequestMapping(value = "league", method = RequestMethod.GET)
    public ModelAndView getStandings(ModelMap model) {
        model.addAttribute("teams",teamBusiness.getLeague());
        return new ModelAndView("league",model);
    }
}
