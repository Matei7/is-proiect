package com.Service;


import com.model.User;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import javax.servlet.FilterChain;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "*")
public class LoginRequiredFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;

        if (isLoginPage(request) && isLoged(request)) {
            request.getRequestDispatcher("/homepage").forward(servletRequest, servletResponse);
        }  else if (!isLoged(request) && !isLoginPage(request)) {
            httpResponse.sendRedirect("/login");
            return;
        } else {

            chain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }


    private boolean isLoged(HttpServletRequest request) {
        return request.getSession().getAttribute("name") != null;
    }

    private boolean isLoginPage(HttpServletRequest request) {
        return request.getServletPath().equals("/login")  || request.getServletPath().equals("/register");
    }

}
