package com.Service;

public class Enums {

    public enum UserType {
        ADMIN, CUSTOMER
    }

    public enum TicketStatus {
        WIN, LOSE, OPENED, PAID, PLACED
    }

    public enum MatchStatus{
        CLOSED, OPEN, INPLAY
    }
}
