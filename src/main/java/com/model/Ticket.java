package com.model;


import com.Service.Enums;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Ticket {
    @Id
    @GeneratedValue
    private int id;

    @ElementCollection
    private Collection<Bet> bets = new ArrayList<>();
    private int userId;
    private double amount;

    private double winningAmount;
    private Enums.TicketStatus status;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Collection<Bet> getBets() {
        return bets;
    }

    public void setBets(Collection<Bet> bets) {
        this.bets = bets;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getWinningAmount() {
        return winningAmount;
    }

    public void setWinningAmount(double winningAmount) {
        this.winningAmount = winningAmount;
    }

    public Enums.TicketStatus getStatus() {
        return status;
    }

    public void setStatus(Enums.TicketStatus status) {
        this.status = status;
    }
}
