package com.model;


import com.Service.Enums;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Match {
    @Id
    @GeneratedValue
    private int id;

    private String homeTeamName;
    private String awayTeamName;

    private Enums.MatchStatus status;
    private int homeTeamGoals;
    private int awayTeamGoals;

    private String startDate;



    @ElementCollection
    private List<String> odds = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHomeTeamName() {
        return homeTeamName;
    }

    public void setHomeTeamName(String homeTeamId) {
        this.homeTeamName = homeTeamId;
    }

    public String getAwayTeamName() {
        return awayTeamName;
    }

    public void setAwayTeamName(String awayTeamId) {
        this.awayTeamName = awayTeamId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getHomeTeamGoals() {
        return homeTeamGoals;
    }

    public void setHomeTeamGoals(int homeTeamGoals) {
        this.homeTeamGoals = homeTeamGoals;
    }

    public int getAwayTeamGoals() {
        return awayTeamGoals;
    }

    public void setAwayTeamGoals(int awayTeamGoals) {
        this.awayTeamGoals = awayTeamGoals;
    }

    public Enums.MatchStatus getStatus() {
        return status;
    }

    public void setStatus(Enums.MatchStatus status) {
        this.status = status;
    }

    public List<String> getOdds() {
        return odds;
    }

    public void setOdds(List<String> odds) {
        this.odds = odds;
    }
}
