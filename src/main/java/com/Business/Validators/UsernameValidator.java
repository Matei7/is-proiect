package com.Business.Validators;

import java.util.regex.Pattern;

import com.model.User;

public class UsernameValidator implements Validator<User>{

    @Override
    public void validate(User t) {
        if (t.getUsername().length() < 4) {
            throw new IllegalArgumentException("Username must be bigger then 4 chars");
        }
        Pattern pattern = Pattern.compile("[A-Za-z0-9_]+");
        if (!pattern.matcher(t.getUsername()).matches()) {
            throw new IllegalArgumentException("Username format incorrect");
        }
    }
}
