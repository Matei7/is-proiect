package com.Business.Validators;

import java.util.regex.Pattern;

public class PasswordValidator  {

    public static final String PASS_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!?*@#$%^&+=])(?=\\S+$).{8,}$";

    public void validate(String username, String password) {
        Pattern pattern = Pattern.compile(PASS_PATTERN);
        if (!pattern.matcher(password).matches()) {
            throw new IllegalArgumentException("Password is not a valid password!");
        }
        if (password.toLowerCase().contains(username.toLowerCase())) {
            throw new IllegalArgumentException("Password can't contain the username");
        }
    }

}
