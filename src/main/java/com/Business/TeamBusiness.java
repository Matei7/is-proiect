package com.Business;

import com.DAO.TeamDAO;

import com.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


@Service
public class TeamBusiness {
    @Autowired
    TeamDAO teamRep;

    public void addTeam(String name){
        Team newTeam = new Team();
        newTeam.setName(name);
        newTeam.setDraws(0);
        newTeam.setGoalsFor(0);
        newTeam.setGoalsAgainst(0);
        newTeam.setLoses(0);
        newTeam.setWins(0);
        newTeam.setPoints(0);

        teamRep.save(newTeam);
    }

    public List<Team> getTeams() {
        List<Team> target = new ArrayList<>();
        teamRep.findAll().forEach(target::add);
        return target;
    }


    public void updateTeam(int id, String newName){
        Team updatedTeam = teamRep.findOne(id);

        updatedTeam.setName(newName);
        teamRep.save(updatedTeam);
    }


    public void deleteTeam(int id){
        teamRep.delete(id);

    }

    public Team getTeam(int id){
        return teamRep.findOne(id);
    }

    public List<Team> getLeague() {
        List<Team> teams = getTeams();
        teams.sort(new Comparator<Team>() {
            @Override
            public int compare(Team o1, Team o2) {
               return o2.getPoints() - o1.getPoints();
            }
        });
        return teams;
    }
}
