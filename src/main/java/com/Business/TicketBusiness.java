package com.Business;


import com.DAO.*;
import com.Service.Enums;
import com.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TicketBusiness {
    @Autowired
    TicketDAO ticketRep;
    @Autowired
    BetDAO betRep;
    @Autowired
    MatchDAO matchRep;
    @Autowired
    UserDAO userRep;
    @Autowired
    TeamDAO teamRep;
    @Autowired
    UserBusiness userBusiness;

    public void addToTicket(int userId, int matchId, String bet) {
        bet = bet.replaceAll(" ", "");
        String[] splitter = bet.split("-");
        Bet newBet = new Bet();
        newBet.setOdd(Double.parseDouble(splitter[1]));
        newBet.setBetResult(splitter[0]);
        newBet.setMatchId(matchId);
        betRep.save(newBet);
        List<Ticket> usersTickets = ticketRep.findByUserIdAndStatus(userId, Enums.TicketStatus.OPENED);
        if (usersTickets.size() == 0) {
            Ticket newTicket = new Ticket();
            newTicket.setStatus(Enums.TicketStatus.OPENED);
            newTicket.setUserId(userId);
            List<Bet> bets = (List) newTicket.getBets();
            bets.add(newBet);
            newTicket.setBets(bets);
            ticketRep.save(newTicket);
        } else {
            Ticket oldTicket = usersTickets.get(0);
            List<Bet> bets = (List) oldTicket.getBets();
            bets.add(newBet);
            oldTicket.setBets(bets);
            ticketRep.save(oldTicket);
        }
    }

    public void calculateMoney(int ticketId,double amount) {
        Ticket ticket = ticketRep.findOne(ticketId);
        double odd = 1;
        for (Bet b : ticket.getBets()) {
            odd = odd * b.getOdd();
        }
        ticket.setWinningAmount(odd * amount);
        ticket.setStatus(Enums.TicketStatus.PLACED);
        ticket.setAmount(amount);
        ticketRep.save(ticket);
    }

    public void setStatus(int ticketId) {
        Ticket ticket = ticketRep.findOne(ticketId);
        ticket.setStatus(Enums.TicketStatus.WIN);
        for (Bet b : ticket.getBets()) {
            String betResult = b.getBetResult();
            Match match = matchRep.findOne(b.getMatchId());
            String result = "";
            if (match.getHomeTeamGoals() == match.getAwayTeamGoals()) {
                result = "X";
            }
            if (match.getHomeTeamGoals() > match.getAwayTeamGoals()) {
                result = "1";
            }
            if (match.getHomeTeamGoals() < match.getAwayTeamGoals()) {
                result = "2";
            }
            if (!result.equals(betResult)) {
                ticket.setStatus(Enums.TicketStatus.LOSE);
            }
        }
        ticketRep.save(ticket);
    }

    public boolean getMatchesStatus(int ticketId) {
        Ticket ticket = ticketRep.findOne(ticketId);
        for (Bet b : ticket.getBets()) {
            Match match = matchRep.findOne(b.getMatchId());
            if (!(match.getStatus().equals(Enums.MatchStatus.CLOSED))) {
                return false;
            }
        }
        return true;

    }

    public int payTickets() {
        List<Ticket> placedTicket = ticketRep.findByStatus(Enums.TicketStatus.PLACED);
        for(Ticket t: placedTicket){
            if(getMatchesStatus(t.getId())){
                setStatus(t.getId());
            }
        }
        List<Ticket> tickets = ticketRep.findByStatus(Enums.TicketStatus.WIN);
        for (Ticket ticket : tickets) {
            User user = userRep.findOne(ticket.getUserId());
            userBusiness.depositFunds(user, ticket.getWinningAmount());
            userBusiness.withdrawFunds(userRep.findByUsername("super").get(0), ticket.getWinningAmount());
            ticket.setStatus(Enums.TicketStatus.PAID);
            ticketRep.save(ticket);
        }
        return tickets.size();
    }

    public boolean existingMatch(int userId, int matchId) {

        List<Ticket> usersTickets = ticketRep.findByUserIdAndStatus(userId, Enums.TicketStatus.OPENED);

        if(usersTickets.size() == 0){
            Ticket newTicket = new Ticket();
            newTicket.setStatus(Enums.TicketStatus.OPENED);
            newTicket.setUserId(userId);
            ticketRep.save(newTicket);
        }
        else {
            for (Bet b : usersTickets.get(0).getBets()) {
                if (b.getMatchId() == matchId) {
                    return true;
                }
            }
        }

        return false;
    }

    public Map<String, String> getTicketContent(int ticketId) {
        Map<String, String> content = new HashMap<>();
        Ticket ticket = ticketRep.findOne(ticketId);
        for(Bet b: ticket.getBets()){
            Match match = matchRep.findOne(b.getMatchId());
            String teams = match.getHomeTeamName()+" - " +match.getAwayTeamName();
            String odds = b.getBetResult() + " - " + b.getOdd();
            content.put(teams,odds);
        }

        return content;
    }

    public Ticket getUserTicket(User user) {
        List<Ticket> found= ticketRep.findByUserIdAndStatus(user.getId(),Enums.TicketStatus.OPENED);
        if(found.size()==0){
            return null;
        }
        return found.get(0);

    }

    public void confirmTicket(int idTicket, double amount){
        Ticket ticket = ticketRep.findOne(idTicket);
        calculateMoney(idTicket,amount);
        User user = userRep.findOne(ticket.getUserId());
        userBusiness.withdrawFunds(user,amount);
        User superUser = userRep.findByUsername("super").get(0);
        userBusiness.depositFunds(superUser,amount);
    }
}
