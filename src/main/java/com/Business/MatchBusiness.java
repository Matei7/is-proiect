package com.Business;

import com.DAO.MatchDAO;
import com.DAO.TeamDAO;
import com.Service.Enums;
import com.model.Match;
import com.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class MatchBusiness {
    @Autowired
    MatchDAO matchRep;
    @Autowired
    TeamDAO teamRep;

    public void addMatch(String homeTeam, String awayTeam, String startDate) {
        Match newMatch = new Match();
        int randomNum;
        List<String> value = new ArrayList<>();
        randomNum = ThreadLocalRandom.current().nextInt(100, 400);
        String s1 = "1 - " + (double) randomNum / 100;
        randomNum = ThreadLocalRandom.current().nextInt(100, 400);
        String s2 = "X - " + (double) randomNum / 100;
        randomNum = ThreadLocalRandom.current().nextInt(100, 400);
        String s3 = "2 - " + (double) randomNum / 100;
        value.add(s1);
        value.add(s2);
        value.add(s3);
        newMatch.setStatus(Enums.MatchStatus.OPEN);
        newMatch.setHomeTeamName(homeTeam);
        newMatch.setAwayTeamName(awayTeam);
        newMatch.setStartDate(startDate);
        newMatch.setOdds(value);
        matchRep.save(newMatch);
    }


    public Match getMatch(int id) {
        return matchRep.findOne(id);
    }

    public List<Match> getOpenMatches() {
        return matchRep.findByStatus(Enums.MatchStatus.OPEN);
    }

    public List<Match> getClosedMatches() {
        return matchRep.findByStatus(Enums.MatchStatus.CLOSED);
    }

    public List<Match> getMatches() {
        List<Match> target = new ArrayList<>();
        matchRep.findAll().forEach(target::add);
        return target;
    }


    public Map<Match, List<String>> getMatchesMap() {
        Map<Match, List<String>> matches = new HashMap<>();
        List<Match> matchesList = getMatches();
        for (Match m : matchesList) {
            matches.put(m, m.getOdds());
        }
        return matches;
    }


    public void simulateMatch(int matchId) {
        Match match = matchRep.findOne(matchId);
        int randomNum;
        randomNum = ThreadLocalRandom.current().nextInt(0, 5);
        match.setAwayTeamGoals(randomNum);
        randomNum = ThreadLocalRandom.current().nextInt(0, 5);
        match.setHomeTeamGoals(randomNum);
        match.setStatus(Enums.MatchStatus.CLOSED);

        Team homeTeam = teamRep.findTeamByName(match.getHomeTeamName()).get(0);
        Team awayTeam = teamRep.findTeamByName(match.getAwayTeamName()).get(0);
        if (match.getHomeTeamGoals() == match.getAwayTeamGoals()) {
            homeTeam.setPoints(homeTeam.getPoints() + 1);
            awayTeam.setPoints((awayTeam.getPoints() + 1));
            homeTeam.setDraws(homeTeam.getDraws() + 1);
            awayTeam.setDraws(awayTeam.getDraws() +1);
        }
        if (match.getHomeTeamGoals() > match.getAwayTeamGoals()) {
            homeTeam.setPoints(homeTeam.getPoints() + 3);
            homeTeam.setWins(homeTeam.getWins() +1);
            awayTeam.setLoses(awayTeam.getLoses()+1);
        }
        if (match.getHomeTeamGoals() < match.getAwayTeamGoals()) {
            awayTeam.setPoints((awayTeam.getPoints() + 3));
            awayTeam.setWins(awayTeam.getWins() +1);
            homeTeam.setLoses(homeTeam.getLoses()+1);
        }
        homeTeam.setGoalsFor(homeTeam.getGoalsFor() + match.getHomeTeamGoals());
        homeTeam.setGoalsAgainst(homeTeam.getGoalsAgainst() + match.getAwayTeamGoals());
        teamRep.save(homeTeam);

        awayTeam.setGoalsFor(awayTeam.getGoalsFor() + match.getAwayTeamGoals());
        awayTeam.setGoalsAgainst(awayTeam.getGoalsAgainst() + match.getHomeTeamGoals());

        teamRep.save(awayTeam);
        matchRep.save(match);
    }

}
