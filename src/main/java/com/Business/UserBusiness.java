package com.Business;

import com.Business.Validators.*;
import com.DAO.UserDAO;
import com.Service.Enums;
import com.Service.PasswordHash;
import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserBusiness {
    List<Validator<User>> validators;
    @Autowired
    UserDAO userRep;
    @Autowired
    PasswordHash passHash;

    public UserBusiness() {
        validators = new ArrayList<>();
        validators.add(new EmailValidator());
        validators.add(new UsernameValidator());
    }


    public void addUser(String username, String password, String name, String addr, String email, String cellphone, Enums.UserType type) throws UnsupportedEncodingException {
        User userToBeAdded = new User();
        userToBeAdded.setUsername(username);
        userToBeAdded.setPassword(passHash.getSha512SecurePassword(password, username.toLowerCase()));
        userToBeAdded.setName(name);
        userToBeAdded.setAddress(addr);
        userToBeAdded.setEmail(email);
        userToBeAdded.setCellphone(cellphone);
        userToBeAdded.setBalance(0);
        userToBeAdded.setType(type);
        for (Validator v : validators) {
            v.validate(userToBeAdded);
        }
        userRep.save(userToBeAdded);
    }


    public void updateUser(HttpServletRequest request, int id, String name, String password, String addr, String cellphone) throws UnsupportedEncodingException {

        HttpSession session = request.getSession();
        session.removeAttribute("name");

        User user = userRep.findOne(id);
        user.setCellphone(cellphone);
        user.setName(name);
        user.setPassword(passHash.getSha512SecurePassword(password, user.getUsername().toLowerCase()));
        user.setAddress(addr);
        session.setAttribute("name",user);
        userRep.save(user);
    }

    public String deleteUser(int id) {
        userRep.delete(userRep.findOne(id));
        return "deleteUser";
    }

    public User getUser(int id) {
        return userRep.findOne(id);
    }

    public User getSuperUser(){
        return userRep.findByUsername("super").get(0);
    }

    public User existingUser(String username, String password) {
        List<User> user = userRep.findByUsernameAndPassword(username, password);
        if (user.size() > 0) {
            return user.get(0);
        }
        return null;
    }

    public void depositFunds(User user, double amount) {
        user.setBalance(user.getBalance() + amount);
        userRep.save(user);

    }

    public void withdrawFunds(User user, double amount) {
        user.setBalance(user.getBalance() - amount);
        userRep.save(user);

    }
}
