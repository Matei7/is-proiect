package com.DAO;

import com.model.League;
import org.springframework.data.repository.CrudRepository;

public interface LeagueDAO extends CrudRepository<League, Integer> {
}
