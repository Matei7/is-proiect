package com.DAO;

import com.model.Team;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TeamDAO extends CrudRepository<Team, Integer> {

    List<Team> findTeamByName(String name);
}
