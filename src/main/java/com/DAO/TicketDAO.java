package com.DAO;

import com.Service.Enums;
import com.model.Ticket;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TicketDAO extends CrudRepository<Ticket, Integer> {

    public List<Ticket> findByUserId(int userId);

    public List<Ticket> findByUserIdAndStatus(int userId, Enums.TicketStatus status);

    public List<Ticket> findByStatus(Enums.TicketStatus status);
}
