package com.DAO;

import com.model.Bet;

import org.springframework.data.repository.CrudRepository;

public interface BetDAO extends CrudRepository<Bet, Integer> {
}
