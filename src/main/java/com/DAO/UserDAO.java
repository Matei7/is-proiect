package com.DAO;

import com.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserDAO extends CrudRepository<User, Integer>{


    List<User> findByUsername(String username);
    List<User> findByUsernameAndPassword(String username, String password);
}
