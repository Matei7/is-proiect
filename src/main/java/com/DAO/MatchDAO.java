package com.DAO;

import com.Service.Enums;
import com.model.Match;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface MatchDAO extends CrudRepository<Match, Integer> {

    public List<Match> findByStatus(Enums.MatchStatus status);

}
