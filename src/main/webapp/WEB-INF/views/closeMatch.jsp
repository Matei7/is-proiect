<%@ page language="java" contentType="text/html"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>
    <%@include file="head.html"%>
</head>
<body>

<%@include file="body2.html"%>
<article>

    <c:if test="${not empty errorStatement}">
        <p style="color:red;" align="center">${errorStatement}</p>
    </c:if>
    <table class="table table-striped table-bordered">
        <thead>
        <th>Match id</th>
        <th>Home team</th>
        <th colspan="2" align="center">End Score</th>
        <th>Away team</th>
        <th>Start Date</th>
        <th>Close Match </th>

        </thead>
        <c:forEach items="${matches}" var="match">
            <tr>

                <td>${match.id}</td>
                <td>${match.homeTeamName}</td>
                <td>${match.homeTeamGoals}</td>
                <td>${match.awayTeamGoals}</td>
                <td>${match.awayTeamName}</td>
                <td>${match.startDate}</td>
                <td>
                    <form action="closeMatch" method="POST">
                        <input type="hidden" name="matchId" value="${match.id}" />
                        <button name="closeButton" type="submit" />
                        Close
                        </button>

                    </form>
                </td>

            </tr>
        </c:forEach>
    </table>




</article>
</div>
</body>
</html>