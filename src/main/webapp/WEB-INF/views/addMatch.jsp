<%@ page language="java" contentType="text/html"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>
    <%@include file="head.html" %>
    <title>Add Account Page</title>
</head>
<body>
<%@include file="body2.html" %>
<article>

    <form action="addMatch" method="POST">
        Home team:  <select id="homeTeam" name="homeTeam">
        <c:forEach items="${teams}" var="team">
            <option value="${team.name }">${team.name}</option>
        </c:forEach> </select>  <br>
        <br>
        Away team :  <select id="awayTeam" name="awayTeam">
        <c:forEach items="${teams}" var="team">
            <option value="${team.name }">${team.name}</option>
        </c:forEach> </select> <br>
        <br>
        Start hour: <input name="startDate" id="startDate" type = "text"  class="form-control" placeholder="insert the hour" required='' />

        <br>
        <input name="insert" type="submit" />
    </form>
</article>
</body>

</html>