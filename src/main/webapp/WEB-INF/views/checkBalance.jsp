<%@ page language="java" contentType="text/html"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>
    <%@include file="head.html" %>
    <title>Add User Page</title>
</head>
<body>
<%@include file="body2.html" %>

<article>

    Full Name: Site Super User<br>
    <br>
    Username:${user.username}<br>
    <br>

    Your balance: ${user.balance} <br>
    <br>


    <c:if test="${not empty ticketsPaid}">
        <p style="color:red;" align="center">${ticketsPaid}</p>
    </c:if>

    <form action="checkBalance" method="POST">
        <button name="payTickets" type="submit" />
        Pay tickets
        </button>

    </form>
</article>

</div>

</body>
</html>