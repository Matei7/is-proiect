<%@ page language="java" contentType="text/html"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>
    <%@include file="head.html"%>
</head>
<body>

<%@include file="body.html"%>
<article>
    <form action="confirmTicket"  method="POST">
    <c:if test="${not empty errorStatement}">
        <p style="color:red;" align="center">${errorStatement}</p>
    </c:if>
    <input name="ticketId" type="hidden" value="${ticketId}" />

    <table class="table table-striped table-bordered">
        <thead>
        <th>Match</th>
        <th>Bet</th>


        </thead>
        <c:forEach items="${content}" var="entry">
            <c:set var = "match" value = "${entry.key}"/>
            <tr>

                <td>${match}</td>
                <td>${entry.value}</td>


            </tr>
        </c:forEach>
    </table>

    <br>
    Amount:<br>
    <input name="amount" type="double"  class="form-control" required=''/><br>

    <button type="submit">Confirm ticket</button>
    </form>



</article>
</div>
</body>
</html>