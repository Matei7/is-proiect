<%@ page language="java" contentType="text/html"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>
    <%@include file="head.html"%>
</head>
<body>

<%@include file="body.html"%>
<article>

    <c:if test="${not empty errorStatement}">
        <p style="color:red;" align="center">${errorStatement}</p>
    </c:if>
    <table class="table table-striped table-bordered">
        <thead>
        <th>Match id</th>
        <th>Home team</th>
        <th colspan="2" align="center">End Score</th>
        <th>Away team</th>
        <th>Start Date</th>
        <th>Bet </th>

        </thead>
        <c:forEach items="${odds}" var="entry">
            <c:set var = "match" value = "${entry.key}"/>
            <tr>

                <td>${match.id}</td>
                <td>${match.homeTeamName}</td>
                <td>${match.homeTeamGoals}</td>
                <td>${match.awayTeamGoals}</td>
                <td>${match.awayTeamName}</td>
                <td>${match.startDate}</td>
                <td>
                    <form action="/addToTicket" method="GET">
                    <select name="odd" >
                    <c:forEach items="${entry.value}" var="bet">
                        <option value="${bet}" >${bet}</option>
                    </c:forEach>
                </select>


                        <input type="hidden" name="matchId" value="${match.id}" />
                        <button name="betButton" type="submit" />
                            Bet
                        </button>

                    </form>
                </td>

            </tr>
        </c:forEach>
    </table>
























</article>
</div>
</body>
</html>