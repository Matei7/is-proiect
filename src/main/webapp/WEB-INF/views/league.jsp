<%@ page language="java" contentType="text/html"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>
    <%@include file="head.html"%>
</head>
<body>

<%@include file="body.html"%>
<article>


    <table class="table table-striped table-bordered css-serial">
        <thead>
        <th>#</th>
        <th>Team</th>
        <th>Wins</th>
        <th>Draws</th>
        <th>Loses</th>
        <th>Goals</th>
        <th>Points</th>
        </thead>
        <c:forEach items="${teams}" var="team">
            <tr>

                <td></td>
                <td>${team.name}</td>
                <td>${team.wins}</td>
                <td>${team.draws}</td>
                <td>${team.loses}</td>
                <td>${team.goalsFor} : ${team.goalsAgainst}</td>
                <td>${team.points}</td>
            </tr>
        </c:forEach>
    </table>
</article>
</div>
</body>
</html>