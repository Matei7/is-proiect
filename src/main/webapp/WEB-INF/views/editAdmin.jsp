<%@ page language="java" contentType="text/html"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>
    <%@include file="head.html" %>
    <title>Add User Page</title>
</head>
<body>
<%@include file="body.html" %>

<article>
    <form action="/editUser2" method="POST">

        <input name="userID" type="hidden" value="${user.id}" />

        Username:<br>
        <input name="username" type="text" value="${user.username}" class="form-control" readonly/><br>

        Password:<br>
        <input name="password" type="password" placeholder="insert password here" class="form-control" required='' /><br>
        Full Name:<br>
        <input name="name" type="text" value="${user.name}" class="form-control" required=''/><br>

        Address:<br>
        <input name="addr" type="text" value="${user.address}" class="form-control" required=''/><br>


        Email:<br>
        <input name="email" type="text" value="${user.email}" class="form-control" readonly/><br>
        CellPhone: <br>
        <input name="cellphone" type="text" value="${user.cellphone}" class="form-control" /><br>

        <input name="insert" type="submit" />

    </form>
</article>

</div>

</body>
</html>