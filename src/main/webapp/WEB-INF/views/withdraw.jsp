<%@ page language="java" contentType="text/html"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>
    <%@include file="head.html" %>
    <title>Add Funds</title>
</head>
<body>
<%@include file="body.html" %>

<article>

    <form action="/withdraw"  method="POST" onSubmit="return validate();">
        <c:if test="${not empty errorStatement}">
            <p style="color:red;" align="center">${errorStatement}</p>
        </c:if>
        Card number: <input name="card" id="card" type = "text" class="form-control" placeholder="Insert your card number" required=''/>
        <br>

        Amount: <input name="amount" id="amount" type = "text"  class="form-control" placeholder="Insert the amount" required='' />

        <br>
        <button type="submit">Withdraw</button>
    </form>
</article>
</body>

</html>