<%@ page language="java" contentType="text/html"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>
    <%@include file="head.html" %>
    <title>Add Funds</title>
</head>
<body>
<%@include file="body.html" %>

<article>

    <form action="/deposit"  method="POST">
        Card number: <input name="card" id="card" type = "text" class="form-control" placeholder="Insert your card number" required=''/>
        <br>
       Valid thru:  <select name="month" >
         <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
        </select>
        /
        <select name="year">
            <option value="2018">2018</option>
            <option value="2019">2019</option>
            <option value="2020">2020</option>
            <option value="2021">2021</option>
        </select>
        &nbsp;
        CVC: <input name="CVC" id="CVC" type = "text" placeholder="Insert CVC. " maxlength="3" required=''/>
        <br>
        <br>
        Holder of card:  <input name="user" id="user" type = "text" class="form-control" placeholder="Insert the name of the holder" required='' />
        <br>
        Amount: <input name="amount" id="amount" type = "text"  class="form-control" placeholder="Insert the amount" required='' />

        <br>
        <button type="submit">Deposit</button>
    </form>
</article>
</body>

</html>